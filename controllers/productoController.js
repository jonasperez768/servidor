const Productos = require("../models/Productos");
const Producto = require("../models/Productos");


exports.crearProducto = async (req, res) =>{
    

    try{
        let producto;
        
        // Creamos nuestro producto
        producto = new Producto(req.body);

        await producto.save();
        res.send(producto);
    
    
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');

    }
}

exports.obtenerProductos = async (req, res) => {

    try {

        const producto = await Producto.find();
        res.json(producto)

    } catch (error){
        console.log(error);
        res.status(500).send('Hubo un error');

    }
}

exports.actualizarProducto = async (req, res) => {

    try {
        const { producto, marca, tipo , cantidad, precio } = req.body;
        let productos = await Producto.findById(req.params.id);

        if(!productos) {
            res.status(404).json({ msg: 'No existe el producto'})
        }

        productos.producto = producto;
        productos.marca = marca;
        productos.tipo = tipo;
        productos.cantidad = cantidad;
        productos.precio = precio;

        productos = await Producto.findOneAndUpdate({_id: req.params.id}, productos, {new: true} )
        res.json(productos);


    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');


    }
}

exports.obtenerProducto = async (req, res) => {

    try {
    
        let productos = await Producto.findById(req.params.id);

        if(!productos) {
            res.status(404).json({ msg: 'No existe el producto'})
        }
        res.json(productos);


    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');


    }
}

exports.eliminarProducto = async (req, res) => {

    try {
    
        let productos = await Producto.findById(req.params.id);

        if(!productos) {
            res.status(404).json({ msg: 'No existe el producto'})
        }

        await Producto.findOneAndRemove({_id: req.params.id })
        res.json({ msg: 'Producto eliminado conexito'});


    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');


    }
}